# Contribute to _Heart Big Query_

If you want to propose new features, bug or security fixes, you have to:
1. Make sure that _Heart Big Query_ is the right repository amongst every _Heart_ modules
2. Code your stuff in a dedicated branch
3. [Optional] Update the README.md
4. Propose your changes through a _Merge request_ (the destination branch must be `master`)

The merging operation will be done by a core team member, once every discussion are resolved.

The core team will then decide if a new version must be release, according to the amount of merge request already merged, the criticity of code...
