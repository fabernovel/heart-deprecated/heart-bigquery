export default class RecordUrl {
  public analyzed: string;
  public report: string;

  constructor(analyzed: string, report: string) {
    this.analyzed = analyzed;
    this.report = report;
  }
}
