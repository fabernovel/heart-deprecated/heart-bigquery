/**
 * Representation of an analysis service in BigQuery
 */
export default class RecordService {
  public name: string;

  constructor(name: string) {
    this.name = name;
  }
}
