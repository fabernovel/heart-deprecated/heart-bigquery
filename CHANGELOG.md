## [2.0.1] - 2019-07-15
### Changed
- Heart wiki url from the README now properly redirect to the repository used before _Heart_ version 3

## [2.0.0] - 2019-06-14
### Added
- Bigquery can now saves normalized ratings from analysis report.
- Bigquery table schema changed to accomodate the new rating (among other things).

### Fixed
- Fix bug where a uselss symlink was created.

## [1.0.0] - 2019-06-04
### Added
- First release (Yay!)
